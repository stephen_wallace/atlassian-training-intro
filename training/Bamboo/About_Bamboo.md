<!SLIDE>
# About Bamboo

Bamboo is a comprehensive Continuous Integration server.
* Works with Git/Mercurial/SVN/CVS repositories
* Can build anything (with some setup)
* Can test anything (with some setup)
* Supports remote build agents
* Capable of managing up to 100 build agents
* Supports staged builds
* Manages dependencies between different build plans
* Capable of interpreting test results and code coverage for tests
* Lets you sleep well at night

~~~SECTION:notes~~~

~~~ENDSECTION~~~

~~~SECTION:handouts~~~


~~~ENDSECTION~~~
