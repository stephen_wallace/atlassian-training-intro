<!SLIDE>
# Overview: About Bamboo
## Objectives
At the end of this lesson, you will be able to:

* Install Bamboo
* Integrate with JIRA
* Set up a local build agent
* Set up a build plan with a single stage
* Set up a build plan with multiple stages
* Set up dependencies between 2 build plans
