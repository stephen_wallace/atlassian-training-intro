<!SLIDE exercise>
# Exercise ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: Bamboo Exercise #

* Objective:
    * Configure Bamboo to start a new build everytime it detects changes in the repository
    * Configure Bamboo to create new branch plans when it detects new branches in the repository 

* Steps:
    * Install Bamboo
    * Integrating with JIRA
    * Integrating with Bitbucket
    * Add a new capability to the default build agent
    * Create a new build plan
    * Put in the repository details
    * Configure the build
    * Run the build
    * Go to Plan Configuration -> Branches
    * Select Create plan branches for matching new branches
    * View the Wallboard

~~~SECTION:notes~~~
#### Discussion Questions
* How would you set up Bamboo to find if your code change affects any other projects?
* Need to know how to configure the build for a basic project
~~~ENDSECTION~~~
