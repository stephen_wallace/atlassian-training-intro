<!SLIDE>
# Puppet Enterprise Stack
## Simplifies installation and configuration.

* Prepackaged PE Stack Elements include:
	* JVM Puppet Master
	* Puppet Agent
	* Puppet Enterprise Console
	* Node Classifier
	* Event Inspector
	* Live Management
	* Puppet Server Metrics
* Automatically configured to scale.
* Fully integrated and tested.
* Enterprise support is included.
