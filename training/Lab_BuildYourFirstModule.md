<!SLIDE lab>
# Lab: Build Your First Module

* Objective:
    * Construct and enforce a Puppet Module to manage a simulated SSH service.

* Steps:
    * Create the module directory structure & support files.
    * Validate the syntax of your user class.

~~~SECTION:notes~~~
Remember that students are working in a simulated environment. They will install
a _simulated_ package and start a _simulated_ service. The filename for the
config file will have to be updated to point to a path they have permissions for.

#### Discussion Questions
* Will `puppet parser validate` tell you if you use the wrong attribute name?
* Will `puppet parser validate` tell you if you misspell the resource type?
* What do you think would happen if you ran `puppet apply` against your new
  module's `manifests/init.pp` manifest?

~~~ENDSECTION~~~

.download users/manifests/init.pp

<!SLIDE lab printonly>
# Lab: Build Your First Module
## Objective:

****

Construct and enforce a Puppet Module to manage a simulated SSH service.

This lab has you creating your first Puppet module. You should create the
appropriate directory structure, which will let Puppet automatically detect the
classes you write. You should create a class to manage the installation and
configuration of SSH and ensure that it is running. You will also need a test
class in order to enforce it.

Remember that packages and services will be simulated but files are not. As such
you will need to adjust the path to the config file to be rooted in your
personal `~/etc` directory.

## Steps:

****

Create your module

1. Create your module directory structure
    * `cd ~/puppet/modules`
    * `mkdir -p ssh/{manifests,tests}`
1. Create an ssh class to manage the resources required.
    * `vim ssh/manifests/init.pp`
    * You will need to manage:
        * Package: `openssh`
        * File: `~/etc/ssh/`
            * tip: `ensure => directory`
        * File: `~/etc/ssh/sshd_config`
        * Service: `sshd`

~~~PAGEBREAK~~~

Validate your module

1. Create a test class to declare your class
    * `vim ssh/tests/init.pp`
    * Simply `include ssh`
1. Use `puppet parser validate` to validate the syntax of your manifests
    * `puppet parser validate ssh/manifests/init.pp`
    * `puppet parser validate ssh/tests/init.pp`
1. Apply your new class manually and observe.
    * `puppet apply ssh/tests/init.pp`

Classify your node and run the agent

1. Classify your agent and run against the master.
    * Browse to your node in the Console and add the `ssh` class to your node.
    * `puppet agent -t`
1. Remove or modify the new `sshd_config` file and enforce configuration again.
    * `rm ~/etc/ssh/sshd_config`
    * `puppet agent -t`

#### Expected result

    [student@training modules]# puppet parser validate ssh/manifests/init.pp
    [student@training modules]# puppet parser validate ssh/tests/init.pp
    [student@training modules]#

There will be no output if the syntax of your file is correct.

~~~PAGEBREAK~~~

#### Discussion Questions

* Will `puppet parser validate` tell you if you use the wrong attribute name?
* Will `puppet parser validate` tell you if you misspell the resource type?
* What do you think would happen if you ran `puppet apply` against your new
  module's `manifests/init.pp` manifest?


<!SLIDE lab printonly>
# Build Your First Module
## Proposed Solution

****

#### Your module structure should resemble

    [student@training modules]# tree ssh/
    ssh/
    ├── manifests
    │   └── init.pp
    └── tests
        └── init.pp


#### Example file: `ssh/manifests/init.pp`

~~~FILE:ssh/manifests/init.pp:Puppet~~~

#### Example file: `ssh/tests/init.pp`

~~~FILE:ssh/tests/init.pp:Puppet~~~
