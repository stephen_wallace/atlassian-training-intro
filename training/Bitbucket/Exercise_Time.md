<!SLIDE exercise>
# Exercise ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: Bitbucket Exercise #

* Objective:
    * Use Bitbucket to create a repository and add users to it

* Steps:
    * Create a new project
    * Create a new repository in your new project
    * Create a new user
    * Give the new user permissions
    * Securing your pull requests

~~~SECTION:notes~~~
#### Discussion Questions
* How might you...
* Why does ....

Pull Requests:
* Requires all tasks to be resolved
* Requires a minimum of 1 successful builds
~~~ENDSECTION~~~
