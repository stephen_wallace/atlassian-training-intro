<!SLIDE>
# Overview: About Bitbucket
## Objectives
At the end of this lesson, you will be able to:

* Install Bitbucket
* Integrate with JIRA
* Create a repository
* Set up users and permissions
* Manage pull requests
