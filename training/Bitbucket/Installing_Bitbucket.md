<!SLIDE>
# Installing Bitbucket
## vagrant up
## Go to My Atlassian - http://my.atlassian.com
## Request for an evaluation license for Bitbucket
## Access Bitbucket via the browser - http://192.168.33.12:7990
## Select a Language, select Internal and click Next
## Select I have a Bitbucket license key and put in the evaluation license
## Put in your user details and password and click Integrate with JIRA
## Fill in the form:
### JIRA base URL: http://192.168.33.12:8080
### JIRA administrator username: <from JIRA install>
### JIRA password: <from JIRA install>
### Uncheck Use JIRA as my user database
## Done (yes it is that quick)
