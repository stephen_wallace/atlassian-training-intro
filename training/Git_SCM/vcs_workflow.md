<!SLIDE>
# Version Control Workflow
## Process

1. Update local working directory.
2. Edit code and make any changes required.
3. Validate and style check code locally.
4. Test code locally
5. Update central server
6. Test on development nodes

~~~SECTION:notes~~~

This slide illustrates HOW to push changes from the developer's workstation
to production. You should elaborate a bit on WHY is critical you do so and
what each step of this workflow will do for you.

Expect a little pushback about how "it's so tedious". You might respond by
listing out some code editor integration, or code review tools, etc. that
make it a very seamless process.

~~~ENDSECTION~~~
