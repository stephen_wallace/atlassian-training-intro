# About "GIT"

WIKIPEDIA:

Git is a mild pejorative with origins in British English for an unpleasant, silly, incompetent, stupid, annoying, senile, elderly or childish person.[2] It is usually an insult, more severe than twit or idiot but less severe than (other naughty words) !

---

Torvalds quipped about the name git (which means "unpleasant person" in British English slang): "I'm an egotistical bastard, and I name all my projects after myself.

*   ...you heard it here first :)
