<!SLIDE lab>
# Lab ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: `git push`

* Objective:
    * Push your local code changes to the master.

* Steps:
    * `cd ~/mycode`
    * Execute `git push origin master`.
        * What is returned?

~~~SECTION:notes~~~
If students are asked for a password, it's because their sshkey wasn't exported and collected
properly. You should ensure that they've run their agent successfully, and then run Puppet on
the master again to collect all exported resources. If all else fails, students may use the
password 'puppetlabs' to complete this lab while you troubleshoot.

#### Discussion Questions
* Does the word _master_ in the `git push` command refer to `master.puppetlabs.vm`? What does it refer to?
* Why did you have to accept an RSA key?
* What happens if you push again? Why?
~~~ENDSECTION~~~

<!SLIDE supplemental exercises>
# Lab ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: `git push`
## Objective:

****

Now that you have one or more commits in your local repository,
push them up to the master. This will execute a hook on the master
to update your environment with your new changes.

## Steps:

****
1. `cd ~/mycode`
2. Execute `git push origin master`
    * What is returned?

### Expected Results

    [root@training mycode]# git push origin master
    The authenticity of host 'master.puppetlabs.vm (10.16.17.129)' can't be established.
    RSA key fingerprint is c0:d8:be:81:8d:f4:96:21:3c:61:aa:d4:a6:7e:fa:2c.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added 'master.puppetlabs.vm,10.16.17.129' (RSA) to the list of known hosts.
    Counting objects: 8, done.
    Compressing objects: 100% (5/5), done.
    Writing objects: 100% (8/8), 1.39 KiB, done.
    Total 8 (delta 0), reused 0 (delta 0)
    remote: Updating modulepath: ben
    remote: Fetching origin
    remote: From /var/repositories/ben
    remote:  * [new branch]      master     -> origin/master
    remote: HEAD is now at 8032d4d initial commit
    remote: Updating list of classes available in the Node Classifier...
    To ben@master.puppetlabs.vm:/var/repositories/ben.git
     * [new branch]      master -> master
 
#### Discussion Questions

* Does the word _master_ in the `git push` command refer to `master.puppetlabs.vm`? What does it refer to?
* Why did you have to accept an RSA key?
* What happens if you push again? Why?
