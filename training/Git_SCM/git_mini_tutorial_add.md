<!SLIDE> 

# `git add`

## `git` stages code to be committed.

* This allows you to iteratively build up a commit.
* You can add files or directories one at a time or many at once.
* You choose which changes in your working directory to commit.

### `git add <file>` adds a file to the staging area:

		@@@ Shell
    [root@training mycode]# git add site.pp
    [root@training mycode]# git status
    # On branch master
    #
    # Initial commit
    #
    # Changes to be committed:
    #   (use "git rm --cached <file>..." to unstage)
    #
    #	new file:   site.pp
    #
