<!SLIDE>

# `git status`

* Tells you the state of your working directory.
* Run this command often, especially before commits.

### Working directory with no changes:

    @@@ Shell
    [root@training mycode]# git status
    # On branch master
    nothing to commit (working directory clean)

### After changes have been made to the working directory:

		@@@ Shell
    [root@training mycode]# git status
    # On branch master
    #
    # Initial commit
    #
    # Untracked files:
    #   (use "git add <file>..." to include in what will be committed)
    #
    #	site.pp
    nothing added to commit but untracked files present (use "git add" to track)

~~~SECTION:handouts~~~
Notice that `git` provides helpful hints as to the suggested next action you might take.
~~~ENDSECTION~~~
