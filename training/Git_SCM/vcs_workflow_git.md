<!SLIDE>
# Git Development Workflow

1. `git pull origin master`
2. Edit, validate, test
3. `git add <code.pp>`
4. `git commit`
5. `git push origin master`
6. Test on development infrastructure

~~~SECTION:notes~~~

This workflow mirrors the generic VCS workflow described earlier and
simply includes the `git` specific commands the user would run.

~~~ENDSECTION~~~
