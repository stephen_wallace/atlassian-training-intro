<!SLIDE>
# Version Control Workflow
## Provides a framework for:

* Safe and recoverable changesets.
* Seamless collaboration with others.
* Viewing complete change history of code.
* Backing out problematic changes.

~~~SECTION:notes~~~
Feel free to go into as much detail as the class is interested in on the
general ideas of version control. Don't talk about `git` specifically
yet, as it is coming up soon.
~~~ENDSECTION~~~
