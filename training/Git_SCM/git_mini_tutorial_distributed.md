<!SLIDE>

# Distributed Version Control

* Instead of *checking out* the current revision, git makes a full *clone* of the entire repository.
* Every user essentially has a full backup of the main server.
    * **No single point of failure**
* Allows disconnected operation, even for commit and diff operations.
* Without network activity, operations are blindingly fast.

![Distributed Workflow](../_images/git_distributed_workflow.png)

~~~SECTION:notes~~~

You might make a joke about committing code on the flight in last night.

The image is the standard Github model, where one repository is chosen as the
canonical upstream. Each developer maintains his/her own fork on Github and
a local clone on their workstation. When they wish to contribute to the master
repository, they will push to their Github repository and submit a pull request
from their public repo to the master repo. The integration manager then has the
option to merge that pull request or reject it.

Upstream changes are pulled from the master repository to each developer's local
clone and pushed back up to their Github repo.

It can be a lot to keep track of, as you know, but many tools exist to make the
process simpler.

~~~ENDSECTION~~~
