<!SLIDE>
# `git push`
## Pushes updates to a remote repository.

* Your *origin* repository is located on the master.
* A `post-update` hook will update the environment working directory.

.break me

		@@@ Shell
    [root@training mycode]# git push origin master
    Counting objects: 3, done.
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 932 bytes, done.
    Total 3 (delta 0), reused 0 (delta 0)
    remote: Updating Puppet Environment training
    remote: From /var/repositories/training
    remote:  * branch            master     -> FETCH_HEAD
    To training@master.puppetlabs.vm:/var/repositories/training.git
     * [new branch]      master -> master		...
