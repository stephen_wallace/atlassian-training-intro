<!SLIDE>

# `git` Mini Tutorial

* Free and open source distributed version control system.
   * Use Git on open or proprietary projects for free, forever.
   * Download, inspect and modify the source code to Git.
* Tiny footprint with lightning fast performance.
    * Nearly all operations are performed locally.
    * Doesn't constantly communicate with a server.
    * Huge speed advantage over centralized systems.
* Cryptographic integrity of every bit of your project is ensured.
    * Every file and commit is checksummed and retrieved by its checksum when checked back out.
    * Assurance that your project is exactly the same as when it was committed and that nothing in its history was changed.

~~~SECTION:notes~~~
This comes directly from the git web pages.
~~~ENDSECTION~~~

~~~SECTION:handouts~~~
Git has rapidly become enormously popular. It is used for very many open source projects as
well as on enterprise level projects. Microsoft and Apple have built git support into their
development tools. GitHub.com provides free hosted git repositories. In short, there's no
reason *not* to learn it!
~~~ENDSECTION~~~
