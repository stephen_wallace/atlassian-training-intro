<!SLIDE>
# More about Git
## More commands and topics you may want to research

* `git diff`
* `git log`
* `git show`
* `git blame <file>`
* `git branch` & `git checkout`

## Resources you may be interested in

* Free online Git book
    * [http://git-scm.com/book](http://git-scm.com/book)
* Learn Git in your browser
    * [http://try.github.com/](http://try.github.com/)

~~~SECTION:notes~~~

This might be a good place to take a moment for discussion. Verify that students
have at least a rudimentary grasp on the concepts, because they'll need to use
`git` to complete labs for the rest of the class. If you have students super
excited about `git` then feel free to spend a couple minutes talking about more
advanced functionality. Be careful not to overwhelm them with the awesome, though.

~~~ENDSECTION~~~
