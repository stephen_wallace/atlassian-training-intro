<!SLIDE>

# `git commit`

## Commits a changeset to your repository.

* After all changed files have been staged with `git add`.
* Takes a cryptographically-verified snapshot of your staged changes.
* Saves a checkpoint into your repository.
* Specify a commit message in one of two ways:
    * Opens your default editor by default.
    * May be passed on the command-line with `-m`.

### `git commit` commits changes to your repository.

		@@@ Shell
    [root@training mycode]# git add site.pp
    [root@training mycode]# git commit -m 'initial commit'
    [master (root-commit) d798484] initial commit
     1 files changed, 44 insertions(+), 0 deletions(-)
     create mode 100644 site.pp

~~~SECTION:handouts~~~
The default editor is configured by setting the `$VISUAL` environment variable.
~~~ENDSECTION~~~
