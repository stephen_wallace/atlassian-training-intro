<!SLIDE lab>
# Lab ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: `git commit`

* Objective:
    * Make your first `git commit`.

* Steps:
    * `cd ~/mycode`
    * Execute `git status`
        * What is git showing you?
    * Add code to your repository with `git add *`
        * What just happened?
    * Commit your code with `git commit -m 'initial commit'`
        * What state is your working directory in now?

~~~SECTION:notes~~~
The students start this lab with an untracked `site.pp` file in their working directory,
placed there by the `classroom` module. This lab will commit it to their repository.

#### Discussion Questions
* What do you think would happen if you ran `git status` in the `modules` subdirectory?
* If you change a file after adding it will those changes be included in your next commit?
* How do you think you might see what the current changeset includes?
~~~ENDSECTION~~~

<!SLIDE supplemental exercises>
# Lab ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: `git commit`
## Objective:

****

Make your first git commit.

Add the `site.pp` file to a git changeset and then commit it.

## Steps:

****
1. `cd ~/mycode`
2. Execute `git status`
    * What is git showing you?
3. Add code to your repository with `git add *`
    * What just happened?
4. Commit your code with `git commit -m 'initial commit'`
    * What state is your working directory in now?

### Expected Results

    [root@training mycode]# git status
    # On branch master
    #
    # Initial commit
    #
    # Untracked files:
    #   (use "git add <file>..." to include in what will be committed)
    #
    #   site.pp
    nothing added to commit but untracked files present (use "git add" to track)
    [root@training mycode]# git add *
    [root@gitstrapagent mycode]# git commit -m 'initial commit'
    [master (root-commit) 8032d4d] initial commit
     4 files changed, 62 insertions(+), 0 deletions(-)
     create mode 100644 .gitignore
     create mode 100644 hiera.yaml
     create mode 100644 hieradata/defaults.yaml
     create mode 100644 manifests/site.pp
     
~~~PAGEBREAK~~~

#### Discussion Questions

* What do you think would happen if you ran `git status` in the `modules` subdirectory?
* If you change a file after adding it will those changes be included in your next commit?
* How do you think you might see what the current changeset includes?

