<!SLIDE center cover noprint>
# Atlassian / Puppet CD Intro
## Stephen Wallace & Bo Wang

![ThirdDan](../_images/DevopsDan-Logo.png)

<!SLIDE center cover printonly>

![ThirdDan](../_images/logo.jpg)

# Atlassian / Puppet CD Intro
# Student Guide
---
## for System Administrators

### Student Guide

#### Devops Dan Education
#### [www.devopsdan.com/learn](www.devopsdan.com/learn)

<!SLIDE center cover supplemental exercises>

![ThirdDan](../_images/logo.jpg)

# Atlassian / Puppet CD Intro
# Supplemental Exercises Guide
---
## for System Administrators

### Supplemental Lab & Exercises Guide

#### Devops Dan Education
#### [www.devopsdan.com/learn](www.devopsdan.com/learn)

<!SLIDE center cover supplemental solutions>

![ThirdDan](../_images/logo.jpg)

# Puppet Fundamentals
---
## for System Administrators

### Supplemental Solutions Guide

#### Devops Dan Education
#### [www.devopsdan.com/learn](www.devopsdan.com/learn)
