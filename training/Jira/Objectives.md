<!SLIDE>
# Overview: Working with JIRA
## Objectives
At the end of this lesson, you will be able to:

* Install JIRA
* Create a project
* Set up versions and components
* Create and manage issues
* Search for issues and set up filters
* Generate a chart from a filter
* Create a dashboard
* Create custom fields

## Advanced Topics (time dependent)

* Customising the workflow
* Setting up notification schemes
* Installing add-ons
