<!SLIDE>
# About JIRA

JIRA is a very versatile issue tracking system!

* Lots of integrations
* Lots of add-ons
* Customisable workflows
* Customisable fields and layouts
* Customisable issue types
* Customisable notifications
* Fine grained permission control (within workflows, within projects, etc.)

~~~SECTION:notes~~~
These notes are only visible by the instructor :)

* Lots of integrations
* Lots of add-ons
* Customisable workflows
* Customisable fields and layouts
* Customisable issue types
* Customisable notifications
* Fine grained permission control (within workflows, within projects, etc.)

~~~ENDSECTION~~~

