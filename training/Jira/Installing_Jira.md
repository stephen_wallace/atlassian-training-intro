<!SLIDE>
# Installing JIRA
## vagrant up
## Go to My Atlassian - http://my.atlassian.com
## Request for an evaluation license for JIRA
## Access JIRA via the browser - http://192.168.33.12:8080
## Select Set it up for me and click Next
## Select I want to use JIRA for software development and click Next
## Put in your email address for http://my.atlassian.com and click Next
## Put in your password for http://my.atlassian.com and click Next
## Click Launch JIRA
## Done (yes it is that quick)
## Log in using your http://my.atlassian.com details
