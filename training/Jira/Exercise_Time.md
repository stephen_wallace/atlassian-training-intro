<!SLIDE exercise>
# Exercise ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: Jira Exercise #

* Objective:
    * Use JIRA to create a project and manage a release

* Steps:
    * Create a project called The Dragon's Lair
    * Create 2 versions, 1.0 and 2.0
    * Create 5 issues, all with a Fix Version of 1.0
        * Prepare weapons
        * Enter Dragon's Lair
        * Defeat Dragon
        * Find Treasure
        * Save Princess
    * Close 3 of the 5 issues
    * Perform a release of version 1.0, when asked what to do with the 2 open issues on the version, choose to move them to version 2.0
    * Go to version 1.0 in the administration interface and generate a report of all of the issues contained in the release

~~~SECTION:notes~~~
#### Discussion Questions
* How would you manage an issue which is reopened?
* What sort of information would you be interested in?
* How creative should I be with my workflows?
~~~ENDSECTION~~~
