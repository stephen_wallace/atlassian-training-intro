class wrappers::staging {
  $version = '8.0.3'
  $source  = 'http://apache.cs.utah.edu/tomcat/tomcat-8'
  $path    = "v${version}/bin/apache-tomcat-${version}.tar.gz"

  staging::file { "apache-tomcat-${version}.tar.gz":
    source => "${source}/${path}",
  }

  staging::extract { "apache-tomcat-${version}.tar.gz":
    target  => '/home/student/usr',
    creates => "/home/student/usr/apache-tomcat-${version}",
    require => Staging::File["apache-tomcat-${version}.tar.gz"],
  }

  file { '/home/student/usr':
    ensure => directory,
  }
}
