# Package File Service
class ntp2 {
  case $::operatingsystem {
    Debian: {
      $packagename = 'openntp'
      $servicename = 'openntp'
    }
    default: {
      $packagename = 'ntp'
      $servicename = 'ntp'
    }
  }

  package { $packagename:
    ensure => installed,
  }

  file { '/etc/ntp.conf':
    ensure  => file,
    source  => 'puppet:///modules/ntp/etc/ntp.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    require => Package[$packagename],
    notify  => Service[$servicename],
  }

  service { $servicename:
    ensure => 'running',
    enable => true,
  }
}
