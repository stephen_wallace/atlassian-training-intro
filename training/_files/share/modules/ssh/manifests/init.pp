class ssh {
  package { 'openssh':
    ensure => present,
  }
  file { '/tmp/sshd_config':
#  file { '/home/student/etc/ssh/sshd_config':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    require => Package['openssh'],
    source  => 'puppet:///modules/ssh/sshd_config',
  }
  service { 'sshd':
    ensure  => running,
    enable  => true,
    require => File['/etc/ssh/sshd_config'],
  }
}

