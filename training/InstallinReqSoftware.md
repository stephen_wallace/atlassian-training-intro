<!SLIDE The Setup...brace yourselves!>
# Setting up the development environment on your laptop

* Objective:
    * Installation and configuration of all required software

* Steps:
    * Install Vagrant
    * Install Virtualbox
    * Install Git for Windows (incl %PATH%)
    * Prepare the necessary code
    * `vagrant up` to create the necessary VM, and boostrap Atlassian s/w
    * Upgrade git
    * Install RVM, and install Ruby v2
    * Install Puppet

~~~SECTION:notes~~~
Apologies we ended up with Ubuntu 12...small, but bad choice!

#### Discussion Questions

* Weapons of choice...why?

~~~ENDSECTION~~~

.download users/manifests/init.pp

#### Discussion Questions


