<!SLIDE>
# The Schedule

* Morning
	* 09.00-09.30am - Intros and overview 
    * 09.30-11.00am - Battle with VMs + installation and integration of Atlassian software
	* 11.00-12.00pm - Basic Atlassian app training

* Lunch
    * 12.00-01.00pm - Stroll into Chatswood. Sandwich provided.

* Afternoon
	* 01.00-01.30pm - Source control process - gitflow
	* 01.30-03.30pm - Puppet Learning VM
	* 03.30-04.00pm - Software testing
	* 04.00-05.00pm - Sewing it all together....
    * 05.00pm       - Wrap up, and ship out!