$(document).ready(function(){
  // This will only load the js in the presenter window
  if (typeof issueUrl !== 'undefined') {
    var componentID    = issueUrl.match(/components=(\d+)/)[1];
    var collectorEmail = (typeof userEmail === 'undefined') ? null : userEmail;
    var collectorName  = (typeof userName  === 'undefined') ? null : userName;

    // If partners can't use the inline collector, send them to the hosted one
    if( collectorEmail !== null && ! collectorEmail.match(/puppetlabs\.com$/) ) {
      var url = 'http://puppetlabs.com/training/issues?';
      issueUrl = url + 'components=' + componentID + '&fullname=' + collectorName + '&email=' + collectorEmail + '&summary=';
    }

    // As always, Google is the most onerous (ridiculous) with regards to security.
    if(window.navigator.vendor != "Google Inc.") {
      jQuery.ajax({
        url: "http://tickets.puppetlabs.com/s/a1cd217e7d553453445bb14bd1008afe-T/en_US-y2pibk/6346/90/1.4.16/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=01105c8a",
        type: "get",
        cache: true,
        dataType: "script"
      });

      window.ATL_JQ_PAGE_PROPS =  {
        triggerFunction: function(showCollectorDialog) {
          jQuery("#report").click(function(e) {
          // This wiggy thing is because the "standard" function cannot access http
          // protocol parent windows, so we have to pre-populate it.
          var summary = 'Issue with slide: ' + $("span#slideFile").text();
            window.ATL_JQ_PAGE_PROPS.fieldValues.summary = summary;

            e.preventDefault();
            showCollectorDialog();
          })
        },
        fieldValues: {
          components : componentID,
          fullname   : collectorName,
          email      : collectorEmail
        }
      };
    }
  }
});
