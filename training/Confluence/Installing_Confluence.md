<!SLIDE>
# Installing Confluence

* **DISCOVER**
* **CONFIGURE**
* and **MANAGE** documentation.

![Installing Confluence](../_images/installing-confluence.png)
