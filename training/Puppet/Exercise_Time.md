<!SLIDE exercise>
# Exercise ~~~SECTION:MAJOR~~~.~~~SECTION:MINOR~~~: Puppet Exercise #

* Objective:
    * Complete the Puppet Learning VM exercise
    * Write your first module!

* Steps:
    * https://puppet.com/download-learning-vm
    * IMPORT into Virtualbox
    * Follow the instuctions :)

~~~SECTION:notes~~~
#### Discussion Questions
* How might you ..?
* Why does Puppet ...blah ?
~~~ENDSECTION~~~
