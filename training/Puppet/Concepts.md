<!SLIDE>
# Puppet Concepts

* The problems
* Traditional solutions / scripts - can get complex
* The Puppet way
* Describing state
* Config drift

~~~SECTION:notes~~~

*    "Concepts/the_problem.md",
*    "Concepts/traditional_solutions.md",
*    "Concepts/traditional_problems.md",
*    "Concepts/shell_script.md",
*    "Concepts/complications.md",
*    "Concepts/puppet_way.md",
*    "Concepts/describe_state.md",
*    "Concepts/drift_graphic.md",
*    "Concepts/assigning_role.md",
*    "Concepts/infrastructure_as_code.md",
*    "Concepts/idempotent.md",
*    "Concepts/language_overview.md",
*    "Concepts/example.md",
*    "Concepts/imperative.md",
*    "Concepts/abstraction.md",
*    "Concepts/RAL_overview.md",
*    "Concepts/type_provider.md",
*    "Concepts/providers.md",
*    "Concepts/package.md",

~~~ENDSECTION~~~
