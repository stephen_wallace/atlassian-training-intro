# Atlassian/Puppet CD intro.

## Laptop requirements:

- Win or Mac
- 8Gb RAM
- At least 10Gb free disk space.
- Administrator access....you'll need to install some software :)

## Accounts required

- Sign up for a Bitbucket account - http://bitbucket.com account
- Sign up for an Atlassian account - http://my.atlassian.com

## Puppet's Learning VM

https://puppet.com/download-learning-vm

Download and test to make sure you can start it.

## Software download and install:

### 1. **Git for Windows - Windows users (only)** https://git-scm.com/download/win

Tip: Git version 1.8.0 or higher is needed.

*Do not forget to add `git` to your Windows path.* This will be required. See [here for some inspiration](http://blog.countableset.ch/2012/06/07/adding-git-to-windows-7-path/)

### 2. **Virtualbox**      - http://download.virtualbox.org/virtualbox/5.0.12/

Download and install the correct version for your platform.

#### 3. Install **Virtualbox guest additions** - http://download.virtualbox.org/virtualbox/5.0.12/Oracle_VM_VirtualBox_Extension_Pack-5.0.12.vbox-extpack

This will allow us to access shared foldered from your laptop.

### 4. **Vagrant**         - https://www.vagrantup.com/downloads.html

  __Download and cache required VM image.....__
```
    1. open a terminal windows / CMD prompt.
    2. create a temp directory and change directory into it
    3. vagrant init precise32 (NB You may need to change this to `vagrant init hashicorp/precise32` depending on the version of Vagrant you have)
```

This will create a single "Vagrantfile" in the current directory.
```
  vagrant up
```
This will connect to the internet, download and cache an image of Ubuntu12. ~300Mb.

Validate that you can see the image once it's finished downloading;
```
09:58:37 {production *+} ~/git/personal/atlassian3-training/tmp$ vagrant box list|grep precise
precise32                 (virtualbox, 0)    <***** success
```
```
  vagrant ssh
```
__Windows users...if this does not work, you likely have either not installed Git for Windows, or not added `git` to your Windows $PATH. See instructions above.__

This will allow you to connect to the now running Ubuntu server. `exit` will disconnect you to the new VIrtualbox running instance, `vagrant halt` will powerdown the VM, and `vagrant destroy` will remove the running VM (vagrant up recreates it on the fly)

## 4. Atlassian goodies....
Should be no need to download these...the `vagrant up` should automatically download them.

## 5. Optional....clone / copy down this repo into a new temp directory

This copy will include the new Vagrantfile, modules and manifests directory.

```
  vagrant up
```

Read the credits.txt file to see the URLs that should now be listening :)

i.e.

JIRA - http://192.168.33.12:8080

Confluence - http://192.168.33.12:8090

Bitbucket - http://192.168.33.12:7990

Bamboo - http://192.168.33.12:8085
